package pl.classroom.util;

import org.hibernate.query.NativeQuery;
import org.junit.Test;

import java.math.BigInteger;
import java.util.List;

import static org.junit.Assert.assertEquals;

public final class HibernateUtilTest {

    @Test
    public void testConnection() {
        // given:
        final var sessionFactory = HibernateUtil.getSessionFactory();
        final var session = sessionFactory.openSession();

        // when:
        final NativeQuery<Object> query = session.createSQLQuery("SELECT 1");
        List<Object> result = query.getResultList();

        // then:
        assertEquals(BigInteger.ONE, result.get(0));
    }
}
