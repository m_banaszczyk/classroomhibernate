package pl.classroom.entity;

import org.junit.Test;
import pl.classroom.util.DateUtil;
import pl.classroom.util.HibernateUtil;

import java.time.LocalDate;

public class _05_ClassroomDaoTest {

    private ClassroomDao dao = new ClassroomDao(HibernateUtil.getSessionFactory());

    @Test
    public void testCreateClassroom(){
        //given -
        Classroom classroom = new Classroom("class A", DateUtil.from(LocalDate.of(2020, 9, 1)),DateUtil.from(LocalDate.of(2020, 6, 30)));

//        when
        dao.begin();
        final var id = dao.save(classroom);
        dao.flushAndClear();
//        dao.rollback();

//        then
        dao.begin();
        final var readClassroom = dao.findById(id);
//                assertEquals(readClassroom);

            dao.commit();
        }
    }
//}
