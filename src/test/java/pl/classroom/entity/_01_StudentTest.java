package pl.classroom.entity;

import org.junit.Test;
import pl.classroom.entity.Student.Gender;
import pl.classroom.util.DateUtil;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import static org.junit.Assert.*;

public final class _01_StudentTest extends BaseEntityTest {

    @Test
    public void testCreateStudent() {
        // given - create student entity
        Student student = new Student("Jan", "Kowalski",
                DateUtil.from(LocalDate.of(1990, 1, 1)), Gender.MALE);

        // when - save student entity
        Serializable id = saveAndFlush(student);

        // then - read student entity & verify
        Student readStudent = getSession().get(Student.class, id);

        assertNotNull(readStudent);
        assertEquals(readStudent, student);
    }

    @Test
    public void testHqlFindByName() {
        // given - example name:
        var name = "Jan";
        // create some students
        Student student0 = new Student("Jan", "Nowak", DateUtil.from(LocalDate.of(1989, 2, 28)), Gender.MALE);
        Student student1 = new Student("Anna", "Kowalska", DateUtil.from(LocalDate.of(1978, 4, 23)), Gender.FEMALE);
        Student student2 = new Student("JAN", "Dziki", DateUtil.from(LocalDate.of(1977, 6, 12)), Gender.MALE);
        Student student3 = new Student("Katarzyna", "jan", DateUtil.from(LocalDate.of(1984, 8, 19)), Gender.FEMALE);

        getSession().save(student0);
        getSession().save(student1);
        getSession().save(student2);
        getSession().save(student3);

        // when - create hql to find students with given "name" (query should check first and lastname)
        List<Student> results = getSession()
                .createQuery("FROM Student WHERE upper(firstName) LIKE :name OR upper(lastname) LIKE :name", Student.class)
                .setParameter("name", name.toUpperCase())
                .getResultList();


        // then - check if results contains students with "name"
        assertFalse(results.isEmpty());
        assertTrue(results.stream().allMatch(student -> student
                .getFullName().toLowerCase().contains(name.toLowerCase())));
        assertEquals(3, results.size());
    }

    @Test
    public void testHqlCountStudentsByGender() {
        // given - example gender
        var gender = Gender.FEMALE;
        // create some students
        Student student0 = new Student("Jan", "Nowak", DateUtil.from(LocalDate.of(1989, 2, 28)), Gender.MALE);
        Student student1 = new Student("Anna", "Kowalska", DateUtil.from(LocalDate.of(1978, 4, 23)), Gender.FEMALE);
        Student student2 = new Student("JAN", "Dziki", DateUtil.from(LocalDate.of(1977, 6, 12)), Gender.MALE);
        Student student3 = new Student("Katarzyna", "jan", DateUtil.from(LocalDate.of(1984, 8, 19)), Gender.FEMALE);

        getSession().save(student0);
        getSession().save(student1);
        getSession().save(student2);
        getSession().save(student3);

        // when - create hql which counts students by gender
        Long count = getSession()
                .createQuery("SELECT count(s) FROM Student s WHERE s.gender like :gender", Long.class)
                .setParameter("gender", gender)
                .getSingleResult();

        // then - check count
        assertTrue(count.compareTo(0L) > 0);
        assertEquals(2, count.intValue());
    }
}
