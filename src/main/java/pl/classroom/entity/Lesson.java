package pl.classroom.entity;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "lesson")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "lesson_type")
public abstract class Lesson {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;



    @Enumerated(EnumType.STRING)
    Subject subject;
    ZonedDateTime date;

    @ManyToMany(cascade = CascadeType.ALL)
    List<Student> participants;

    public Lesson() {}

    public Lesson(ZonedDateTime date, Subject subject) {
        this.date = date;
        this.subject = subject;
        this.participants = new ArrayList<>();
    }

    public Long getId() {
        return id;
    }

    public ZonedDateTime getDate() {
        return date;
    }

    public Subject getSubject() {
        return subject;
    }

    public boolean isPlannedOn(LocalDate date) {
        return this.date.toLocalDate().equals(date);
    }

    public boolean isPlannedAtTime(ZonedDateTime dateTime) {
        return this.date.equals(dateTime);
    }

    public List<Student> getParticipants() {
        return participants;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Lesson lesson = (Lesson) o;
        return id == lesson.id &&
                date.equals(lesson.date) &&
                subject == lesson.subject;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, date, subject);
    }

    public void addParticipant(Student student) {
        if (!participants.contains(student)) {
            participants.add(student);
        }
    }
}