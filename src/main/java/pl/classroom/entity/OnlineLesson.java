package pl.classroom.entity;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.time.ZonedDateTime;
import java.util.Objects;

@Entity
@DiscriminatorValue("Online")

public final class OnlineLesson extends Lesson{

    @Column(length = 80)
    String url;
    @Column(length = 20)
    String code;

    public OnlineLesson(ZonedDateTime data, Subject subject, String url, String code) {
        super(data, subject);
        this.url = url;
        this.code = code;
    }

    private OnlineLesson() {
    }

    public String getUrl() {
        return url;
    }

    public String getCode() {
        return code;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OnlineLesson that = (OnlineLesson) o;
        return url.equals(that.url) &&
                code.equals(that.code);
    }

    @Override
    public int hashCode() {
        return Objects.hash(url, code);
    }

    @Override
    public String toString() {
        return "OnlineLesson{" +
                "url='" + url + '\'' +
                ", code='" + code + '\'' +
                ", subject=" + subject +
                ", date=" + date +
                ", participants=" + participants +
                '}';
    }

}
