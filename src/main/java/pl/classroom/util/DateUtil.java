package pl.classroom.util;

import java.time.*;

public final class DateUtil {
    public static ZonedDateTime from(LocalDate date) {
        return ZonedDateTime.of(date, LocalTime.MIDNIGHT, ZoneId.systemDefault());

    }
    public static ZonedDateTime from(LocalDateTime date){
        return ZonedDateTime.of(date, ZoneId.systemDefault());
    }
}
